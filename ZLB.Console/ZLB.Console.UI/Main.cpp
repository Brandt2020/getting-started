
#include <iostream> 
#include <conio.h> 


using namespace std;

int main()
{
	// Initialize variable for user input
	int userInput;

	// Initial string displayed
	cout << "Enter an integer between 1 and 5:";

	//Accept user input, looking for an integer between 1 and 5
	cin >> userInput;

	// Set up counter to repeat quote string equal to number the user entered
	int counter = 0;

	// if the condition is not met, an error message will show
	if (userInput > 5 || userInput < 1)
	{
		cout << userInput << " is not a valid integer";
	}
	else {

		// Repeat quote equal to the valid number the user entered
		while (counter < userInput)
		{

			cout << "\"Those who dare to fail miserably can acheive greatly.\"\t -John F. Kennedy\n";
			counter++;

		}
	}

	_getch();
	return 0;
}