
#include <iostream>
#include <conio.h>

using namespace std;

//Function prototype
void SayHi();


//Byref is indicated by &
int AddIntegers(int &num1, int num2);

//function (not a method)
int main()
{
	SayHi();

	int userInput;
	cin >> userInput;


	cout << " + 4 = " << AddIntegers(userInput, 4);


	(void)_getch();
	return 0;

}

void SayHi()
{

	cout << "Hello\n";
}


int AddIntegers(int &num1, int num2)
{

	return num1 + num2;
}