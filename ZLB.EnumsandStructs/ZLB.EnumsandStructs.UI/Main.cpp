// Structs and Enums Demo
// Zachary Brandt

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

enum Role {Student, Instructor, Admin};

struct Person
{
	//data members (aka field)
	string FirstName;
	string LastName;
	float GPA = 0;
	Role Role = Student;
};
// Use & for byref so we don't have to copy EVERYTHING, const so we won't change zach
void PrintPersonInfo(const Person& s)
{
	if (s.Role == Student)
	{
	
	cout << s.FirstName << " " << s.LastName << ": " << s.GPA << "\n";
}
	else
	{
		cout << s.FirstName << " " << s.LastName << " is not a student.\n";
	}
}

//enum DoorState { Closed, Opening, Open, Closing};


int main()
{

	//Struct example
	Person zach;

	zach.FirstName = "Zach";
	zach.LastName = "Brandt";
	zach.GPA = 4.0f;
	zach.Role = Student;

	PrintPersonInfo(zach); // pass a struct to a function
	
	//DoorState door1 = Open;
	//if (door1 == Open)
	//{
	//}

	(void)_getch();
	return 0;
}