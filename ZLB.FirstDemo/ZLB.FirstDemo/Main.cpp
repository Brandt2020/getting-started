
#include <iostream> // can use cout (&cin)
#include <conio.h> // so we can use _getch()

int main()
{ 
	std::cout << "Hello World." << std::endl;

	

	int i = 6; // whole #'s (+ and -)
	float f = 2.3; // decimal #s
	double PI = 3.1415926535;
	const double PI = 3.1415926535; //const can't be changed later

	bool ZachIsCool = true;

	//string is not primitive!

	//casting

	f = (float)i; // we can store 6 in a float (6.0)
	i = (int)f; //




	//modulus (mod)

	std::cout << 10 % 3;

	//if statement doesn't not have to be boolean
	if (i % 2 == 0) std::cout << i << " is odd. \n";
	else std::cout << i << " is even. \n";

	char c = _getch(); //hold the window open
	return 0; // no error
}